Front - Bemacash
====================

Objetivo é mostrar a utilização do AngularJS (typescript).

# Tecnologias utilizadas

*NPM - v6.10.3*

*Angular - v6.1.1*

## Executar serviço

### Etapa 1

Precisa levantar o serviço da `api` que está no projeto `https://bitbucket.org/allysonsantos/api-bemacash/`. Só seguir as instruções do `readme`.

### Etapa 2

```
git clone https://allysonsantos@bitbucket.org/allysonsantos/front-bemacash.git
```
```
cd front-bemacash
```
```
npm install
```
```
ng serve
```
```
Acessar URL: http://localhost:4200
```

### Observação:

Caso queira executar `build` do projeto, precisa utilizar esse comando:

```
ng build --prod --base-href=/bemacash/
```

**IMPORTANTE:** Nesse caso precisa pegar todos os arquivos que estão dentro da pasta `dist/bemacash` e colocar dentro de uma pasta raiz como, por exemplo, `localhost` de um servidor `apache`. Desta forma acessando a URL: http://localhost/