import { Routes } from '@angular/router'

import { HomeComponent } from './home/home.component'
import { PedidosComponent } from './pedidos/pedidos.component'
import { PedidoDetalheComponent } from './pedido-detalhe/pedido-detalhe.component'

export const ROUTES: Routes = [
    { path: '', component: HomeComponent },
    { path: 'pedidos', component: PedidosComponent },
    { path: 'pedidos/:id', component: PedidoDetalheComponent }
]