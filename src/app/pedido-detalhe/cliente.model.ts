export interface Cliente {
    id: string
    nome: string
    cnpj: string
    estado: string
    telefone: string
    cep: string
    endereco: string
    pais: string
    criado: string
    preco: number
}