import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router'

import { PedidosService } from '../pedidos/pedidos.service'

import { Cliente } from '../pedido-detalhe/cliente.model'
import { Produto } from '../pedido-detalhe/produto.model'
import { Historico } from '../pedido-detalhe/historico.model'

@Component({
  selector: 'be-pedido-detalhe',
  templateUrl: './pedido-detalhe.component.html',
  styleUrls: ['./pedido-detalhe.component.css']
})
export class PedidoDetalheComponent implements OnInit {

  cliente: Cliente
  produtos: Produto[] = []
  historicos: Historico[] = []

  constructor(private PedidosService: PedidosService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.PedidosService.clientePedidoPorId(this.route.snapshot.params['id']).subscribe(cliente => this.cliente = cliente)

    this.PedidosService.produtoPedidoPorId(this.route.snapshot.params['id']).subscribe(produtos => this.produtos = produtos)

    this.PedidosService.historicoPedidoPorId(this.route.snapshot.params['id']).subscribe(historicos => this.historicos = historicos)
  }

}
