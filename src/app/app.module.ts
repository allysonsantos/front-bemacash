import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';

import { ROUTES } from './app.routes'

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { PedidosComponent } from './pedidos/pedidos.component';
import { RouterModule } from '@angular/router';
import { PedidoComponent } from './pedidos/pedido/pedido.component';
import { PedidosService } from './pedidos/pedidos.service';
import { PedidoDetalheComponent } from './pedido-detalhe/pedido-detalhe.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    PedidosComponent,
    PedidoComponent,
    PedidoDetalheComponent
  ],
  imports: [
    HttpModule,
    BrowserModule,
    RouterModule.forRoot(ROUTES)
  ],
  providers: [PedidosService],
  bootstrap: [AppComponent]
})
export class AppModule { }
