import { Injectable } from '@angular/core'
import { Http } from '@angular/http'

import { Observable } from 'rxjs'
import { map } from 'rxjs/operators'

import { Pedido } from './pedido/pedido.model'
import { Cliente } from '../pedido-detalhe/cliente.model'
import { Produto } from '../pedido-detalhe/produto.model'
import { Historico } from '../pedido-detalhe/historico.model'

import { BEMACASH_API } from '../app.api'


@Injectable()
export class PedidosService {

    constructor(private http: Http) { }

    pedidos(): Observable<Pedido[]> {
        return this.http.get(`${BEMACASH_API}/listarpedidos`).pipe(map(response => response.json()))
    }

    clientePedidoPorId(id: string): Observable<Cliente> {
        return this.http.get(`${BEMACASH_API}/obterclientepedido/${id}`).pipe(map(response => response.json()))
    }

    produtoPedidoPorId(id: string): Observable<Produto[]> {
        return this.http.get(`${BEMACASH_API}/obterprodutopedido/${id}`).pipe(map(response => response.json()))
    }

    historicoPedidoPorId(id: string): Observable<Historico[]> {
        return this.http.get(`${BEMACASH_API}/obterhistoricopedido/${id}`).pipe(map(response => response.json()))
    }
}