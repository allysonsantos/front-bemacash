export interface Pedido {
    id: string
    empresa: string
    status: string
    atualizado: string
}