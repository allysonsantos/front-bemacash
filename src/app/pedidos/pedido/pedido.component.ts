import { Component, OnInit, Input } from '@angular/core';

import { Pedido } from './pedido.model'

@Component({
  selector: 'be-pedido',
  templateUrl: './pedido.component.html',
  styleUrls: ['./pedido.component.css']
})
export class PedidoComponent implements OnInit {

  @Input() pedido: Pedido

  constructor() { }

  ngOnInit() {
  }

}
