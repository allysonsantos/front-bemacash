import { Component, OnInit } from '@angular/core';

import { Pedido } from './pedido/pedido.model'
import { PedidosService } from './pedidos.service'

@Component({
  selector: 'be-pedidos',
  templateUrl: './pedidos.component.html',
  styleUrls: ['./pedidos.component.css']
})
export class PedidosComponent implements OnInit {

  pedidos: Pedido[] = []

  constructor(private PedidosService: PedidosService) { }

  ngOnInit() {
    this.PedidosService.pedidos().subscribe(pedidos => this.pedidos = pedidos)
  }

}
